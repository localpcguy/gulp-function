define(["require", "exports", "./gulpfunction.plugins", "typings-global"], function (require, exports, plugins) {
    "use strict";
    module.exports = function (functionsToExecuteArg, executionModeArg) {
        if (executionModeArg === void 0) { executionModeArg = 'forEach'; }
        //important vars
        var executionMode = executionModeArg; //can be forEach or atEnd
        var functionsToExecute = functionsToExecuteArg;
        var promiseArray = [];
        var runFunction = function (functionArg, file, enc, cb) {
            var returnValue = functionArg();
            if (typeof returnValue !== "undefined" && typeof returnValue.then !== "undefined") {
                promiseArray.push(returnValue);
            }
        };
        var checkAndRunFunction = function (file, enc, cb) {
            if (typeof functionsToExecute === "function") {
                runFunction(functionsToExecute, file, enc, cb);
            }
            else if (Array.isArray(functionsToExecute)) {
                for (var anyFunction in functionsToExecute) {
                    runFunction(functionsToExecute[anyFunction], file, enc, cb);
                }
            }
            else {
                throw new Error("gulp-callfunction: something is strange with the given arguments");
            }
            return plugins.Q.all(promiseArray);
        };
        var hasExecutedOnce = false;
        var forEach = function (file, enc, cb) {
            switch (executionMode) {
                case "forEach":
                    checkAndRunFunction(file, enc, cb).then(function () {
                        cb(null, file);
                    });
                    break;
                case "forFirst":
                    !hasExecutedOnce ? checkAndRunFunction(file, enc, cb).then(function () {
                        cb(null, file);
                    }) : cb(null, file);
                    hasExecutedOnce = true;
                    break;
                case "atEnd":
                    cb(null, file);
                    break;
                default:
                    break;
            }
        };
        var atEnd = function (cb) {
            if (executionMode === "atEnd") {
                checkAndRunFunction(null, null, cb).then(function () {
                    cb();
                });
            }
            else {
                cb();
            }
        };
        return plugins.through2.obj(forEach, atEnd);
    };
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi90cy9pbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztJQU9BLE1BQU0sQ0FBQyxPQUFPLEdBQUcsVUFBVSxxQkFBK0IsRUFBQyxnQkFBbUM7UUFBbkMsZ0NBQW1DLEdBQW5DLDRCQUFtQztRQUMxRixnQkFBZ0I7UUFDaEIsSUFBSSxhQUFhLEdBQUcsZ0JBQWdCLENBQUMsQ0FBQyx5QkFBeUI7UUFDL0QsSUFBSSxrQkFBa0IsR0FBRyxxQkFBcUIsQ0FBQztRQUMvQyxJQUFJLFlBQVksR0FBRyxFQUFFLENBQUM7UUFDdEIsSUFBSSxXQUFXLEdBQUcsVUFBUyxXQUFXLEVBQUUsSUFBSSxFQUFFLEdBQUcsRUFBRSxFQUFFO1lBQ2pELElBQUksV0FBVyxHQUFHLFdBQVcsRUFBRSxDQUFDO1lBQ2hDLEVBQUUsQ0FBQyxDQUFDLE9BQU8sV0FBVyxLQUFLLFdBQVcsSUFBSSxPQUFPLFdBQVcsQ0FBQyxJQUFJLEtBQUssV0FBVyxDQUFDLENBQUMsQ0FBQztnQkFDaEYsWUFBWSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUNuQyxDQUFDO1FBQ0wsQ0FBQyxDQUFDO1FBRUYsSUFBSSxtQkFBbUIsR0FBRyxVQUFVLElBQUksRUFBRSxHQUFHLEVBQUUsRUFBRTtZQUM3QyxFQUFFLENBQUMsQ0FBQyxPQUFPLGtCQUFrQixLQUFLLFVBQVcsQ0FBQyxDQUFDLENBQUM7Z0JBQzVDLFdBQVcsQ0FBQyxrQkFBa0IsRUFBRSxJQUFJLEVBQUUsR0FBRyxFQUFFLEVBQUUsQ0FBQyxDQUFDO1lBQ25ELENBQUM7WUFBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLENBQUMsQ0FBQztnQkFDM0MsR0FBRyxDQUFDLENBQUMsSUFBSSxXQUFXLElBQUksa0JBQWtCLENBQUMsQ0FBQyxDQUFDO29CQUN6QyxXQUFXLENBQUMsa0JBQWtCLENBQUMsV0FBVyxDQUFDLEVBQUUsSUFBSSxFQUFFLEdBQUcsRUFBRSxFQUFFLENBQUMsQ0FBQztnQkFDaEUsQ0FBQztZQUNMLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDSixNQUFNLElBQUksS0FBSyxDQUFDLGtFQUFrRSxDQUFDLENBQUM7WUFDeEYsQ0FBQztZQUNELE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUN2QyxDQUFDLENBQUM7UUFFRixJQUFJLGVBQWUsR0FBRyxLQUFLLENBQUM7UUFDNUIsSUFBSSxPQUFPLEdBQUcsVUFBVSxJQUFJLEVBQUUsR0FBRyxFQUFFLEVBQUU7WUFDakMsTUFBTSxDQUFDLENBQUMsYUFBYSxDQUFDLENBQUEsQ0FBQztnQkFDbkIsS0FBSyxTQUFTO29CQUNWLG1CQUFtQixDQUFDLElBQUksRUFBRSxHQUFHLEVBQUUsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDO3dCQUNwQyxFQUFFLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO29CQUNuQixDQUFDLENBQUMsQ0FBQztvQkFDSCxLQUFLLENBQUM7Z0JBQ1YsS0FBSyxVQUFVO29CQUNYLENBQUMsZUFBZSxHQUFHLG1CQUFtQixDQUFDLElBQUksRUFBRSxHQUFHLEVBQUUsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDO3dCQUN2RCxFQUFFLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO29CQUNuQixDQUFDLENBQUMsR0FBRyxFQUFFLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUFDO29CQUNwQixlQUFlLEdBQUcsSUFBSSxDQUFDO29CQUN2QixLQUFLLENBQUM7Z0JBQ1YsS0FBSyxPQUFPO29CQUNSLEVBQUUsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7b0JBQ2YsS0FBSyxDQUFDO2dCQUNWO29CQUNJLEtBQUssQ0FBQztZQUNkLENBQUM7UUFDTCxDQUFDLENBQUM7UUFFRixJQUFJLEtBQUssR0FBRyxVQUFTLEVBQUU7WUFDbkIsRUFBRSxDQUFDLENBQUMsYUFBYSxLQUFLLE9BQU8sQ0FBQyxDQUFDLENBQUM7Z0JBQzVCLG1CQUFtQixDQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDO29CQUNyQyxFQUFFLEVBQUUsQ0FBQztnQkFDVCxDQUFDLENBQUMsQ0FBQztZQUNQLENBQUM7WUFBQyxJQUFJLENBQUMsQ0FBQztnQkFDSixFQUFFLEVBQUUsQ0FBQztZQUNULENBQUM7UUFDTCxDQUFDLENBQUM7UUFDRixNQUFNLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsT0FBTyxFQUFDLEtBQUssQ0FBQyxDQUFDO0lBQy9DLENBQUMsQ0FBQyJ9